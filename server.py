#!/usr/bin/python3

from concurrent.futures import process
import keyboard
import numpy as np
import socket
import threading
import time

serverHost = '127.0.0.1'
serverPort = 8787

class Server:
    def __init__(self):
            hostname = socket.gethostname()
            print("hostname:" + hostname)
            self.ip = socket.gethostbyname(hostname)
            while 1:
                try:

                    self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.s.bind((serverHost, serverPort))
                    break
                except:
                    print("Couldn't bind to that port")

            self.connections = []
            self.accept_connections()

    def accept_connections(self):
        self.s.listen(100)

        print('Running on IP: '+self.ip)

        while True:
            c, addr = self.s.accept()
            self.connections.append(c)
            threading.Thread(target=self.handle_client,args=(c,addr,), daemon=True).start()

    # Handles a client when it connects
    def handle_client(self,c,addr):
            while 1:
                try:
                    data = c.recv(1024)
                    self.broadcast(c, data)
                    if keyboard.is_pressed("z"): # this stuf can probably be removed
                            print("q pressed, ending handle_client loop")
                            break
                except socket.error:
                    c.close()

    # send data from one client to all other clients
    def broadcast(self, sock, data):
        for client in self.connections:
            # if client != self.s and client != sock: # Comment this out to send to self
                try:
                    # Mess with data here for different languages
                    processed_data = fantasy_vocode(data)
                    client.send(processed_data)

                    if keyboard.is_pressed("z"): # this stuf can probably be removed
                        print("q pressed, ending broadcast loop")
                        break
                except OSError as e:
                    if keyboard.is_pressed("z"): # this stuf can probably be removed
                        print("q pressed, ending broadcast error loop")
                        print(e)
                        break
                    pass

# Mess with data here for different languages
def fantasy_vocode(data):
    np_data = np.frombuffer(data, dtype=np.int16)

    # Get volume
    # volume = int(np.amax(np_data) / 32767 * 100)
    # print(f'{volume}%')

    # Change pitch (https://github.com/symphonly/figaro/blob/master/res/filters/pitch.py#L40)
    fac = -5 # Pitch change amount (-5 to +5 sounds ok)
    freq = np.fft.rfft(np_data)
    N = len(freq)
    sh_freq = np.zeros(N, freq.dtype)
    S = int(np.round(fac if fac > 0 else N + fac, 0))
    s = int(N - S)
    sh_freq[:S] = freq[s:]
    sh_freq[S:] = freq[:s]
    sh_chunk = np.fft.irfft(sh_freq)
    processed_data = sh_chunk.astype(np_data.dtype)

    # Process each datapoint individually
    # processed_data = []
    # for b in np_data:
    #     """
    #     * and / are for gain and soften
    #     """
    #     processed_data.append(b)

    return np.array(processed_data, dtype=np.int16, order='C').tobytes(order='C')

def host_server():
    Server()

# Local Client to kill the server.accept
class Client:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            self.s.connect((serverHost, serverPort))
        except Exception as e :
            print("Couldn't connect to server")
            print(e)
            return
        print("Connected to Server")


try:
    threading.Thread(target=host_server, args=(), daemon=True).start()
    while 1:
        time.sleep(1)
except KeyboardInterrupt:
    killer = Client()
    print("received Ctrl+C, quitting")
